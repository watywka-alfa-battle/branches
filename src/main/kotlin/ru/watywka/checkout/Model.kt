package ru.watywka.checkout

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.sql.Date
import java.sql.Time


@Table("branches")
data class Branch(@Id val id: Int, val title: String, val lon: Double, val lat: Double, val address: String)

@Table("queue_log")
data class Queue(@Id val id: Int,
                 @Column("branches_id") val branchesId: Int,
                 val data: Date,
                 @Column("start_time_of_wait") val startTimeOfWait: Time,
                 @Column("end_time_of_wait") val endTimeOfWait: Time,
                 @Column("end_time_of_service") val endTimeOfService: Time)