package ru.watywka.checkout

import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingle
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.apache.commons.math3.stat.descriptive.rank.Median
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.server.bodyValueAndAwait
import org.springframework.web.reactive.function.server.coRouter
import kotlin.math.*

@SpringBootApplication
class CheckoutApplication

fun main(args: Array<String>) {
    runApplication<CheckoutApplication>(*args)
}

@Configuration
class Config {

    @Bean
    fun routerFunction(branchRepository: BranchRepository, queueRepository: QueueRepository) = coRouter {
        "branches".nest {
            "/{id}".nest {
                GET("") {
                    val id = it.pathVariable("id").toIntOrNull()
                            ?: return@GET status(HttpStatus.BAD_REQUEST).bodyValueAndAwait(Error("not an integer"))
                    val branch = branchRepository.findById(id).awaitFirstOrNull()
                    if (branch != null) {
                        ok().bodyValueAndAwait(branch)
                    } else {
                        status(HttpStatus.NOT_FOUND).bodyValueAndAwait(Error("branch not found"))
                    }
                }
                GET("/predict") {
                    val id = it.pathVariable("id").toIntOrNull()
                            ?: return@GET status(HttpStatus.BAD_REQUEST).bodyValueAndAwait(Error("not an integer"))
                    val dayOfWeek = it.queryParam("dayOfWeek").map { it.toIntOrNull() }.orElseGet{null}
                            ?: return@GET status(HttpStatus.BAD_REQUEST).bodyValueAndAwait(Error("not an integer"))
                    val hourOfDay = it.queryParam("hourOfDay").map { it.toIntOrNull() }.orElseGet{null}
                            ?: return@GET status(HttpStatus.BAD_REQUEST).bodyValueAndAwait(Error("not an integer"))
                    val b = branchRepository.findById(id).awaitFirstOrNull()
                    if (b != null) {
                        val queues = queueRepository.getAll(b.id, dayOfWeek, hourOfDay).map { queue ->
                            ((queue.endTimeOfWait.time - queue.startTimeOfWait.time).toDouble()/ 1000) }.collectList().awaitFirst()
                        if ( queues != null) {
                            val median = Median()
                            median.data = ArrayList(queues).toDoubleArray()
                            ok().bodyValueAndAwait(BranchWithTime(b.id,
                            b.title,
                            b.lon,
                            b.lat,
                            b.address,
                            dayOfWeek,
                            hourOfDay,
                            median.evaluate().toInt()))
                        } else {
                            status(HttpStatus.BAD_REQUEST).bodyValueAndAwait(Error("no information"))
                        }
                    } else {
                        status(HttpStatus.NOT_FOUND).bodyValueAndAwait(Error("branch not found"))
                    }
                }
            }

            GET("") {
                val lat = it.queryParam("lat").get().toDouble()
                val lon = it.queryParam("lon").get().toDouble()
                val b = branchRepository.findAll().reduce { t: Branch?, u: Branch? ->
                    if (t == null || u == null) {
                        u
                    } else {
                        if (distance(t.lon, t.lat, lon, lat) < distance(u.lon, u.lat, lon, lat)) t else u
                    }
                }.awaitSingle()
                val body = BranchWithDistance(b.id,
                        b.title,
                        b.lon,
                        b.lat,
                        b.address,
                        distance(b.lon, b.lat, lon, lat).toInt())
                ok().bodyValueAndAwait(body)
            }

        }
    }
}

data class Error(val status: String)

data class BranchWithDistance(val id: Int, val title: String, val lon: Double, val lat: Double, val address: String, val distance: Int)

data class BranchWithTime(val id: Int,
                          val title: String,
                          val lon: Double,
                          val lat: Double,
                          val address: String,
                          val dayOfWeek: Int,
                          val hourOfDay: Int,
                          val predicting: Int)

fun distance(lon1: Double, lat1: Double, lon2: Double, lat2: Double): Double {
    val psi1 = lon1 * PI / 180
    val psi2 = lon2 * PI / 180
    val phi1 = lat1 * PI / 180
    val phi2 = lat2 * PI / 180

    val phiSin = abs(sin((phi1 - phi2) / 2))
    return 2 * 6371 * 1000 * asin(phiSin * sqrt(1 + cos(phi1) * cos(phi2) * (sin((psi1 - psi2) / 2) / phiSin).pow(2)))
}
