package ru.watywka.checkout

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux

@Repository
interface BranchRepository : ReactiveCrudRepository<Branch, Int>

@Repository
interface QueueRepository : ReactiveCrudRepository<Queue, Int> {
    @Query("select * from queue_log where branches_id = :id and date_part('dow', data) = :day AND date_part('hour', end_time_of_wait) = :hour")
    fun getAll(id: Int, day: Int, hour: Int) : Flux<Queue>
}